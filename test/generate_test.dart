import 'package:test/test.dart';
import '../bin/generate.dart';

void main() {
  group('Decompose package import', () {
    setUpAll(() {});

    tearDownAll(() {});

    setUp(() {});

    tearDown(() {});

    test('package', () {
      // given
      final import1 =
          "import 'package:takkan_kitchensink/app/config/takkan.dart';";
      // when
      final d1 = decomposePackageImportStatement(import1);
      // then

      expect(d1.isPackage, true);
      expect(d1.isDart, false);
      expect(d1.packageName, 'takkan_kitchensink');
      expect(d1.path, 'app/config/takkan.dart');
    });
    test('dart', () {
      // given
      final import2 = "import 'dart:convert';";
      // when
      final d2 = decomposePackageImportStatement(import2);
      // then

      expect(d2.isPackage, false);
      expect(d2.isDart, true);
      expect(d2.packageName, 'convert');
      expect(d2.path, '');
    });
    test('relative path', () {
      // given
      final import3 = "'../config/other.dart';";
      // when
      final d3 = decomposePackageImportStatement(import3);
      // then

      expect(d3.isPackage, false);
      expect(d3.isDart, false);
      expect(d3.packageName, 'n/a');
      expect(d3.path, '../config/other.dart');
    });
  });
}
