import 'dart:convert';
import 'dart:io';
import 'package:io/io.dart';
import 'package:path/path.dart' as path;

/// Used by the developer to generate server side code from an app's PScript and PSchema
/// definitions.
///
/// It copies the companion package 'takkan_server_code_generator' from GitLab,
/// inserts the PScript and PSchema instances from the target app, and compiles.
///
/// The result is an instance of 'takkan_server_code_generator' with the required
/// Script and Schema embedded.
///
/// The code generator is then invoked to output server code.
/// params (defined as param=value on the command line):
/// - project: path to the root of the target project
/// - instance: relates to the *takkan.json* file of the target app, and identifies which instance
/// the code is being generated for
/// - serverCodeDir: (optional) directory to generate the server code into.  By default this
/// is ${user.home}/b4a/$projectName/$instance
///
void main(List<String> arguments) async {
  final args = ConnectArgs(args: arguments);
  if (args.missingKeys.isNotEmpty) {
    final msg =
        'These parameters are required but missing: ${args.missingKeys.join(',')}';
    print(msg);
  } else {
    final tempInstanceDir = await createTempInstanceDir(args);
    await copyGeneratorSource(args, tempInstanceDir);
    await copyScriptFiles(args, tempInstanceDir);
    final compiledGenerator = await compileGenerator(tempInstanceDir);
    await invokeGenerator(compiledGenerator, args);
  }
}

Future<Directory> createTempInstanceDir(ConnectArgs args) async {
  final randomTemp = await Directory.systemTemp.createTemp();
  final instanceTemp =
      Directory('${randomTemp.path}/${args.projectName}/${args.instance}');
  return instanceTemp.create(recursive: true);
}

/// Makes a local copy now - replace later with download from gitLab
/// Maybe cache anyway?
Future<bool> copyGeneratorSource(
    ConnectArgs args, Directory takkanGenDir) async {
  final generatorSourceProject =
      Directory('../git/takkan/takkan_server_code_generator');
  await copyPath(generatorSourceProject.path, takkanGenDir.path);
  return true;
}

Future<File> compileGenerator(Directory tempInstanceDir) async {
  final pGenFile = File('${tempInstanceDir.path}/pgen');
  /// This looks weird - but it seems to need potentially multiple goes at this!
  int loops=0;
  int compileExitCode=-99;
  while (compileExitCode !=0 && loops < 5){
    await doPubGet(tempInstanceDir);
    compileExitCode=await doCompile(tempInstanceDir,pGenFile);
    loops++;
  }
  print((compileExitCode == 0)
      ? 'generator compiled'
      : 'failed with exit code: $exitCode');
  return pGenFile;
}

Future<int> doCompile(Directory tempInstanceDir, File pGenFile) async {

  print('Compiling generator');
  var process = await Process.start('bash', [
    '-c',
    'dart compile exe ${tempInstanceDir.path}/lib/generate.dart -o ${pGenFile.path}'
  ]);
  process.stdout.transform(utf8.decoder).forEach(print);
  process.stderr.transform(utf8.decoder).forEach(print);
  final exitCode = await process.exitCode;
  return exitCode;
}

Future<int> doPubGet(Directory tempInstanceDir) async {
  final msg='Executing \'dart pub get\' in ${tempInstanceDir.path}';
  var process = await Process.start('bash', ['-c', 'dart pub get'],workingDirectory: tempInstanceDir.path);
  process.stdout.transform(utf8.decoder).forEach(print);
  process.stderr.transform(utf8.decoder).forEach(print);
  return await process.exitCode;
}

Future<bool> invokeGenerator(File generator, ConnectArgs args) async {
  final List<String> params = ['serverCodeDir=${args.serverCodeDir}'];
  final paramString = params.join(' ');
  print('Invoking generator at ${generator.path} with params: $paramString');
  var process = await Process.start(generator.path, params);
  process.stdout.transform(utf8.decoder).forEach(print);
  var exitCode = await process.exitCode;
  print((exitCode == 0)
      ? 'Code generation complete'
      : 'failed with exit code: $exitCode');
  return exitCode == 0;
}

/// Read script related files form the project being generated.  This must
/// contain a file '/lib/app/loaders.dart' to act as the start point
copyScriptFiles(ConnectArgs args, Directory takkanGenDir) async {
  final projectExists = args.projectRoot.existsSync();
  if (!projectExists) {
    print('Directory ${args.projectRoot.path} does not exist');
    return;
  }
  print('Collating script files from: ${args.projectRoot.path}');
  final File loadersFile =
      File('${args.projectRoot.path}/lib/app/loaders.dart');
  if (!loadersFile.existsSync()) {
    print('Project must contain a file at: /lib/app/loaders.dart');
    return;
  }
  print('Processing loaders.dart');
  await processFile(loadersFile, args, takkanGenDir);
}

/// A recursive process to copy script related files from the project being generated,
/// 'following' imports and copying them as well.  This process excludes
/// core takkan imports.
///
/// Each [file] is copied to [takkanGenDir], and its import statements
/// amended to reflect its new file position.
///
/// Its imports are 'followed' by identifying their files and passing them to this
/// function.
Future<bool> processFile(
    File file, ConnectArgs args, Directory takkanGenDir) async {
  final sourceRoot = Directory('${args.projectRoot.absolute.path}/lib');
  final destRoot = Directory('${takkanGenDir.absolute.path}/lib/target');
  if (!await destRoot.exists()) {
    destRoot.create(recursive: true);
  }

  final destFile = _translateSourceToDestination(file, sourceRoot, destRoot);
  if (!await destFile.exists()) {
    await destFile.create(recursive: true);
  }
  final copiedFile = await file.copy(destFile.absolute.path);
  await updateImportStatements(copiedFile, sourceRoot, args.projectName);
  final importsToCopy = await identifyImports(file, sourceRoot);
  for (Import importFile in importsToCopy) {
    await processFile(importFile.file, args, takkanGenDir);
  }
  return true;
}

File _translateSourceToDestination(
    File file, Directory sourceRoot, Directory destRoot) {
  String sourceFileRelative =
      file.absolute.path.replaceFirst(sourceRoot.path, '');
  return File('${destRoot.absolute.path}$sourceFileRelative');
}

/// Amends import statements in newly copied [file] to reflect its new position
/// Import statements that reference the target app are amended to reference the
/// new, temporary generator.
///
/// For example, with 'takkan_kitchensink' as the target app:
/// ```
/// import 'package:takkan_kitchensink/app/config/schema.dart';
/// ```
///
/// becomes
///
/// ```
/// import 'package:takkan_server_code_generator/app/config/schema.dart';
/// ```
Future<int> updateImportStatements(
    File file, Directory sourceRoot, String targetAppName) async {

  final lines = await file.readAsLines();
  final imports = await identifyImports(file, sourceRoot);
  final selfRefer="import 'package:$targetAppName";
  final redirect="import 'package:takkan_server_code_generator/target";
  for (Import import in imports) {
    final redirected=  import.statement.replaceFirst(selfRefer, redirect);
    final index = lines.indexOf(import.statement);
    lines[index] = redirected;
  }

  await file.writeAsString(lines.join('\n'));
  return 1;
}

/// Identify imports in the file which need to be included.
/// All [takkanCorePackages] are ignored
Future<List<Import>> identifyImports(File file, Directory sourceRoot) async {
  final List<Import> output = List.empty(growable: true);
  final lines = await file.readAsLines();
  final imports =
      lines.where((element) => element.trim().startsWith('import ')).toList();
  imports.removeWhere((element) => _isCorePackage(element));

  for (String import in imports) {
    String imp = import.replaceAll(';', '');
    imp = imp.replaceAll("'", '');
    final s = imp.split('/').toList(growable: true);
    s.removeAt(0);
    final relativeFilePath = s.join('/');
    output.add(Import(
      file: File('${sourceRoot.absolute.path}/$relativeFilePath'),
      statement: import,
    ));
  }
  return output;
}

bool _isCorePackage(String element) {
  return decomposePackageImportStatement(element).isCore;
}

DecomposedImport decomposePackageImportStatement(String element) {
  final s0 = element.replaceFirst('import', '');
  final s1 = s0.replaceAll(';', '');
  final s2 = s1.trimLeft();
  final s3 = s2.replaceAll('\'', '');
  final s4 = s3.split('/');
  final s5 = s4[0].split(':');
  final isPackage = s5[0] == 'package';
  final isDart = s5[0] == 'dart';

  if (isPackage || isDart) {
    final path = s4.sublist(1).join('/');
    final packageName = s5[1];
    return DecomposedImport(
        isDart: isDart,
        isPackage: isPackage,
        packageName: packageName,
        path: path);
  } else {
    final path = s4.join('/');
    return DecomposedImport(isDart: isDart, isPackage: isPackage, path: path);
  }
}

class DecomposedImport {
  final bool isPackage;
  final String packageName;
  final String path;
  final bool isDart;

  DecomposedImport(
      {required this.isPackage,
      this.packageName = 'n/a',
      required this.path,
      this.isDart = false});

  bool get isCore => isDart || takkanCorePackages.contains(packageName);
}

/// This should be in a utils package somewhere, it is used in takkan_server_code_generator and takkan_dev_app
class Args {
  final Map<String, String> mappedArgs = {};
  final List<String> missingKeys = List.empty(growable: true);

  Args({required List<String> args, List<String> requiredKeys = const []}) {
    for (String element in args) {
      final a = element.split('=');
      mappedArgs[a[0]] = a[1];
    }
    validatePresent(keys: requiredKeys);
  }

  List<String> validatePresent({required List<String> keys}) {
    for (String key in keys) {
      if (!mappedArgs.containsKey(key)) {
        missingKeys.add(key);
      }
    }
    return missingKeys;
  }
}

class ConnectArgs {
  final Args args;

  ConnectArgs({required List<String> args})
      : args = Args(args: args, requiredKeys: ['project']);

  Directory get projectRoot {
    final d = Directory(args.mappedArgs['project'] ?? '.');
    return Directory(path.normalize(d.absolute.path));
  }

  String get projectName => projectRoot.path.split('/').last;

  String get instance => args.mappedArgs['instance'] ?? 'dev';

  List<String> get missingKeys => args.missingKeys;

  String get serverCodeDir {
    final serverC = args.mappedArgs['serverCodeDir'];
    if (serverC == null) {
      final String userHome = Platform.environment['HOME'] ??
          Platform.environment['USERPROFILE'] ??
          'User Home not identified';
      return '$userHome/b4a/$projectName/$instance';
    } else {
      return serverC;
    }
  }
}

/// Simple data class to record both an import statement and the file to which it refers
class Import {
  final File file;
  final String statement;

  const Import({required this.file, required this.statement});
}

const takkanCorePackages = <String>[
  'takkan_back4app_client',
  'takkan_client',
  'takkan_back4app_server',
  'takkan_client',
  'takkan_dev_app',
  'takkan_script',
  'takkan_backend',
  'takkan_generator',
];
